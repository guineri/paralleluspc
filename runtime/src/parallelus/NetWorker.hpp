
/*                                                _    __ ____
 *   _ __  ___ _____   ___   __  __   ___ __     / |  / /  __/
 *  |  _ \/ _ |  _  | / _ | / / / /  / __/ /    /  | / / /__
 *  |  __/ __ |  ___|/ __ |/ /_/ /__/ __/ /__  / / v  / /__
 *  |_| /_/ |_|_|\_\/_/ |_/____/___/___/____/ /_/  /_/____/
 *
 */

#ifndef parallelus_NETWORKER_HPP
#define parallelus_NETWORKER_HPP

#include <condition_variable>
#include <memory>
#include <stdexcept>
#include <thread>
#include <jni.h>
#include <parallelus/Kernel.hpp>
#include <parallelus/Task.hpp>

#include "util/error.h"

namespace parallelus {

/**
 * This class manages a threads that executes tasks supplied by the scheduler and send its 
 *
 * @author Guilherme Andrade
 */
class NetWorker {
    int _newsockfd;
    std::mutex _mutex;
    std::condition_variable _cv;
    std::condition_variable _desableCV;
    std::string _kernelsource;
    bool _kill;
    bool _running;
    JNIEnv *_env;
    bool _desable;
    unsigned int _id;

    std::shared_ptr<Device> _device;

    void executeTask(std::unique_ptr<Task> task) {
        ERROR_PRINTER_FUNC("[NETWORKER]Task execution initialized.");
        task->createExternKernels(_env);
        task->sendKernelsName(_newsockfd);
        task->callConfigFunction(_device,_newsockfd);
        task->runExtern(_newsockfd);	
        task->callFinishFunction(_device,_newsockfd);
        ERROR_PRINTER_FUNC("[NETWORKER]Task execution finished.");
    }

public:
    /**
     * Constructs the NetWorker from the given device.
     */
    NetWorker(int newsockfd, std::string kernelsource, std::shared_ptr<Device> device, int id) : _newsockfd(newsockfd), 
				_kernelsource(kernelsource), _kill(false), _running(false), _desable(false), _id(id), _device(device) {

    }

    ~NetWorker() {
        /// Kills the NetWorker, but only if it was without anything to do.
        /// This will block until the NetWorker is killed.
        std::unique_lock<std::mutex> lock(_mutex);
        _kill = true;
        _cv.notify_one();
        _desable = false;
        _desableCV.notify_one();
    }

    /**
     * Starts the NetWorker. If the NetWorker has already started this function does
     * nothing.
     * The NetWorker asks for work to the scheduler until the sheduler returns a
     * nullptr pointer to a task. When this happens, it enters into sleep until
     * it is waken up by wakeUp() or killed by kill() by the runtime.
     */
    template<class Scheduler>
    void run(std::shared_ptr<Scheduler> scheduler, JavaVM *jvm) {
        if(_running)
            return;
        _running = true;

        std::thread t([=] () mutable {
            _env = nullptr;

            if(jvm) {
                if(jvm->AttachCurrentThread((void **) &_env, nullptr))
                //if(jvm->AttachCurrentThread(&_env, nullptr))
                    throw std::runtime_error("failed to attach thread to JVM.");
                _device->setJNIEnv(_env);    
            }

            ERROR_PRINTER_FUNC("[NETWORKER]NetWorker Running: %d", _newsockfd);
            std::unique_lock<std::mutex> lock(_mutex);
           
            for(;;) {	
                ERROR_PRINTER_FUNC("[NETWORKER]Taking task.");
                auto task = scheduler->netpop();
                ERROR_PRINTER_FUNC("[NETWORKER]Returned netpop");
                if(task) {
                    ERROR_PRINTER_FUNC("[NETWORKER]ExecuteTask");
                    executeTask(std::move(task));
                    ERROR_PRINTER_FUNC("[NETWORKER]Executed.");
                    continue;
                }
                if(_kill)
                    break;

                if(_desable)
                    _desableCV.wait(lock);

                _cv.wait(lock);
            }

            if(jvm)
                jvm->DetachCurrentThread();
        });
        t.detach();
    }
	
	int getSocketNumber(){
		return _newsockfd;
	}
		
    /// Waits for the NetWorker to finish.
    inline void finish() {
        std::unique_lock<std::mutex> lock(_mutex);
        if(_desable)
            enable();
    }

    inline void enable() {
        _desable = false;
        _desableCV.notify_one();
        _cv.notify_one();
    }

    inline void desable() {
        std::unique_lock<std::mutex> lock(_mutex);
        _desable = true;
        _cv.notify_one();
    }

    inline unsigned int id() {
        return _id;
    }

    inline void wakeUp() {
        _cv.notify_one();
    }

    inline void kill(){
         std::unique_lock<std::mutex> lock(_mutex);
        _kill = true;
        _cv.notify_one();       
    }
    
};

}

#endif // !parallelus_NETWORKER_HPP
