/*                                                _    __ ____
 *   _ __  ___ _____   ___   __  __   ___ __     / |  / /  __/
 *  |  _ \/ _ |  _  | / _ | / / / /  / __/ /    /  | / / /__
 *  |  __/ __ |  ___|/ __ |/ /_/ /__/ __/ /__  / / v  / /__
 *  |_| /_/ |_|_|\_\/_/ |_/____/___/___/____/ /_/  /_/____/
 *
 */

#ifndef parallelus_EXECUTIONCONTROLER_HPP
#define parallelus_EXECUTIONCONTROLER_HPP

#include <condition_variable>
#include <memory>
#include <stdexcept>
#include <thread>
#include <jni.h>

namespace parallelus {
class Worker;

class ExecutionControler {
    std::mutex _mutex;
    std::condition_variable _cv;
    bool _kill;
    bool _running;
    std::vector<std::shared_ptr<Worker>> _workers;
    std::shared_ptr<Scheduler> _scheduler;
    std::shared_ptr<Scheduler> _netScheduler;

   
public:
    /**
     * Constructs the EXECUTIONCONTROLER from the given device.
     */
    ExecutionControler(std::vector<std::shared_ptr<Worker>> workers, std::shared_ptr<Scheduler> scheduler,
    				   std::shared_ptr<Scheduler> netScheduler): _kill(false), _running(false), _workers(workers),
    				   											  _scheduler(scheduler), _netScheduler(netScheduler) {

    }

    ~ExecutionControler() {
        /// Kills the EXECUTIONCONTROLER, but only if it was without anything to do.
        /// This will block until the EXECUTIONCONTROLER is killed.
        std::unique_lock<std::mutex> lock(_mutex);
        _kill = true;
        _cv.notify_one();
    }

    /**
     * Starts the EXECUTIONCONTROLER. If the EXECUTIONCONTROLER has already started this function does
     * nothing.
     * The EXECUTIONCONTROLER asks for work to the scheduler until the sheduler returns a
     * nullptr pointer to a task. When this happens, it enters into sleep until
     * it is waken up by wakeUp() or killed by kill() by the runtime.
     */
    void run(JavaVM *jvm, int numWorkers) {
        if(_running)
            return;
        _running = true;

        std::thread t([=] () mutable {
            JNIEnv *env = nullptr;

            if(jvm) {
                if(jvm->AttachCurrentThread((void **)&env, nullptr)){
                //if(jvm->AttachCurrentThread(&env, nullptr))
                    throw std::runtime_error("failed to attach thread to JVM.");
                }
            }

            std::unique_lock<std::mutex> lock(_mutex);

 			//printf("[JNI] Execution Controler Initited\n");

 			for(auto &worker : _workers){
 				if((int)worker->id() >= numWorkers){
       				worker->desable();
                }
                //else{
                //   printf("[CONF] Worker setup %d\n", worker->id());
                //}
   			}



            if(jvm){
                jvm->DetachCurrentThread();
            }
        });
        t.detach();
    }

    /// Waits for the EXECUTIONCONTROLER to finish.
    inline void finish() {
        std::unique_lock<std::mutex> lock(_mutex);
    }

    /**
     * Wakes up the EXECUTIONCONTROLER if it was sleeping because it didn't have anything
     * to do.
     */
    inline void wakeUp() {
        _cv.notify_one();
    }

    inline void kill(){
        std::unique_lock<std::mutex> lock(_mutex);
        _kill = true;
        _cv.notify_one();
    }
};

}

#endif // !parallelus_EXECUTIONCONTROLER_HPP
