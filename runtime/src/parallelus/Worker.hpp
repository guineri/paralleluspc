/*                                                _    __ ____
 *   _ __  ___ _____   ___   __  __   ___ __     / |  / /  __/
 *  |  _ \/ _ |  _  | / _ | / / / /  / __/ /    /  | / / /__
 *  |  __/ __ |  ___|/ __ |/ /_/ /__/ __/ /__  / / v  / /__
 *  |_| /_/ |_|_|\_\/_/ |_/____/___/___/____/ /_/  /_/____/
 *
 */

#ifndef parallelus_WORKER_HPP
#define parallelus_WORKER_HPP

#include <condition_variable>
#include <memory>
#include <stdexcept>
#include <thread>
#include <jni.h>
#include <parallelus/Device.hpp>
#include <parallelus/Kernel.hpp>
#include <parallelus/Task.hpp>

namespace parallelus {
class Device;


/**
 * This class manages a threads that executes tasks supplied by the scheduler.
 *
 * @author Renato Utsch
 */
class Worker {
    std::shared_ptr<Device> _device;
    std::mutex _mutex;
    std::condition_variable _cv;
    std::condition_variable _desableCV;
    bool _kill;
    bool _running;
    bool _desable;
    unsigned int _id;

    struct timeval tv;

    /// Executes a given task.
    double getCurrentTime(){
        gettimeofday(&tv,0);
        return tv.tv_sec + tv.tv_usec/1.e6;  
    }

    void executeTask(std::unique_ptr<Task> task) {
        //double t1 = getCurrentTime();
        task->createKernels(_device);
        task->callConfigFunction(_device,0);
        task->run();
        task->callFinishFunction(_device,0);
        task->setTimeFinExec(); 
        //double t2 = getCurrentTime();
        //printf("%.8f\n", t2 - t1);
        _device->finish();
    }

public:
    /**
     * Constructs the worker from the given device.
     */
    Worker(std::shared_ptr<Device> device, int id) : _device(device), _kill(false),
            _running(false), _desable(false), _id(id) {

    }

    ~Worker() {
        /// Kills the worker, but only if it was without anything to do.
        /// This will block until the worker is killed.
        std::unique_lock<std::mutex> lock(_mutex);
        _kill = true;
        _desable = false;
        _cv.notify_one();
        _desableCV.notify_one();
    }

    /**
     * Starts the worker. If the worker has already started this function does
     * nothing.
     * The worker asks for work to the scheduler until the sheduler returns a
     * nullptr pointer to a task. When this happens, it enters into sleep until
     * it is waken up by wakeUp() or killed by kill() by the runtime.
     */
    template<class Scheduler>
    void run(std::shared_ptr<Scheduler> scheduler, JavaVM *jvm) {
        if(_running)
            return;
        _running = true;

        std::thread t([=] () mutable {
            JNIEnv *env = nullptr;

            if(jvm) {
                if(jvm->AttachCurrentThread((void **)&env, nullptr)){
                //if(jvm->AttachCurrentThread(&env, nullptr))
                    throw std::runtime_error("failed to attach thread to JVM.");
                }
                _device->setJNIEnv(env);
            }

            std::unique_lock<std::mutex> lock(_mutex);

            for(;;) {
                auto task = scheduler->pop(*_device);

                if(task) {
                    //printf("Worker %d Executing\n", _id);
                    task->setTimeInitExec();
                    executeTask(std::move(task));
                    continue;
                }
                if(_kill){
                    break;
                }

                if(_desable)
                    _desableCV.wait(lock);

                _cv.wait(lock);
            }

            if(jvm){
                jvm->DetachCurrentThread();
            }
        });
        t.detach();
    }

    /// Waits for the worker to finish.
    inline void finish() {
        std::unique_lock<std::mutex> lock(_mutex);
        if(_desable)
            enable();
    }

    /**
     * Wakes up the worker if it was sleeping because it didn't have anything
     * to do.
     */
    inline void wakeUp() {
        _cv.notify_one();
    }

    inline void enable() {
        _desable = false;
        _desableCV.notify_one();
        _cv.notify_one();
    }

    inline void desable() {
        std::unique_lock<std::mutex> lock(_mutex);
        _desable = true;
        _cv.notify_one();
    }

    inline unsigned int id() {
        return _id;
    }

    inline void kill(){
        std::unique_lock<std::mutex> lock(_mutex);
        _kill = true;
        _cv.notify_one();
    }
};

}

#endif // !parallelus_WORKER_HPP
